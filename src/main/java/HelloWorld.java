import cassandra.PersonId;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteException;
import org.apache.ignite.Ignition;

import java.sql.Date;

public class HelloWorld {
    public static void main(String[] args) throws IgniteException {
        try (Ignite ignite = Ignition.start("examples/config/cassandra/cassandra.xml")) {
            // Put values in cache.
            IgniteCache<Integer, PersonId> cache = ignite.getOrCreateCache("myCache");
//
            Date date = new Date(2000,12,1);
            PersonId personId = new PersonId("babak","ahmadi",12,true,12L,13, date);
            cache.put(1,personId);
//            cache.put(2, "World!");

            // Get values from cache and
            // broadcast 'Hello World' on all the nodes
            // in the cluster.
            ignite.compute().broadcast(() -> {
                PersonId hello = cache.get(1);
                System.out.println(hello);
//                String world = cache.get(2);

//                System.out.println(hello + " " + world);
            });
        }
    }
}