package mysql;

public class Person {
    private long id;
    private long orgId;
    private String name;
    private int salary;

    public long getId() {
        return id;
    }

    public long getOrgId() {
        return orgId;
    }

    public int getSalary() {
        return salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public void setOrgId(long orgId) {
        this.orgId = orgId;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Person(){}
    public Person(long id, long orgId , String name , int salary){
        this.id = id;
        this.orgId = orgId;
        this.name = name;
        this.salary = salary;
    }
}
